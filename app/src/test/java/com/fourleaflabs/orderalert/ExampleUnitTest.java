package com.fourleaflabs.orderalert;

import com.fourleaflabs.orderalert.tasks.OrderNoteUpdateTask;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    @Test
    public void showMeTime() {

        OrderNoteUpdateTask task = new OrderNoteUpdateTask(null, null, null);
        String current_time = task.getTimeString();

        System.out.println("Time: " + current_time);

        assertTrue(true);
    }

}