package com.fourleaflabs.orderalert;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class DashboardActivity extends Activity {

    private WebView dashboard_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        String url = getIntent().getStringExtra("url");
        dashboard_view = (WebView) findViewById(R.id.DashboardView);
        dashboard_view.setWebViewClient(new WebViewClient());
        WebSettings webSettings = dashboard_view.getSettings();
        webSettings.setJavaScriptEnabled(true);
        dashboard_view.loadUrl(url);

    }
}
