package com.fourleaflabs.orderalert.interfaces;

import com.android.volley.VolleyError;

public interface RequestCallback{
    // TODO: create generic type
    void onSuccessInt(int result);
    void onSuccessString(String result);
    void onError(VolleyError error);
}