package com.fourleaflabs.orderalert.interfaces;

/**
 * Created by android6 on 23/12/14.
 * We implement this and call back from non activity class to activity
 */
public interface CallBackActivity {
    void CallBackActivity(int Message);
}
