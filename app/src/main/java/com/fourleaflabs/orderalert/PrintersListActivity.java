package com.fourleaflabs.orderalert;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.clover.sdk.util.CloverAccount;
import com.clover.sdk.v1.ResultStatus;
import com.clover.sdk.v1.ServiceConnector;
import com.clover.sdk.v1.printer.Category;
import com.clover.sdk.v1.printer.Printer;
import com.clover.sdk.v1.printer.PrinterConnector;
import com.fourleaflabs.orderalert.addapters.PrinterAdapter;
import com.fourleaflabs.orderalert.interfaces.CallBackActivity;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by android6 on 23/12/14.
 */
public class PrintersListActivity extends Activity implements ServiceConnector.OnServiceConnectedListener,CallBackActivity {
    public static final String EXTRA_PRINTER = "Printer";
    public static final int INTENT_REQUEST_CODE =5265;
    private static final String TAG = "PrinterTestActivity";
    private static final int REQUEST_ACCOUNT = 0;
    private static final Random RANDOM = new Random(SystemClock.currentThreadTimeMillis());
    public static String ActionString="com.fourleaflabs.orderalert.printer.order.listactivity";
    private PrinterConnector connector;
    private Account account;
    private TextView Loading;
    private Button Cancel;
    private ListView list;
    private PrinterAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_printer_list);
        Loading=(TextView) findViewById(R.id.OrderAlert_PrinterLoading);
        Cancel=(Button) findViewById(R.id.OrderAlert_PrinterCancel);
        list=(ListView) findViewById(R.id.OrderAlert_PrinterList);

        Loading.setVisibility(View.VISIBLE);
        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED,getIntent());
                finish();
            }
        });
        startAccountChooser();
    }

    private static String getStatusString(String status, ResultStatus resultStatus) {
        return "<" + status + " " + (resultStatus != null ? resultStatus : "") + ": " + DateFormat.getDateTimeInstance().format(new Date()) + ">";
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (account != null) {
            connect();

        }
    }

    private void GetPRinterList() {
        Loading.setText(getResources().getString(R.string.OrderAlert_PrinterLoading));
        Loading.setVisibility(View.VISIBLE);
        connector.getPrinters(new PrinterConnector.PrinterCallback<List<Printer>>() {
            @Override
            public void onServiceSuccess(List<Printer> result, ResultStatus status) {
                super.onServiceSuccess(result, status);
               // updatePrinters("get printers success", status, result);

                if (result!=null && !result.isEmpty()) {
                  //  editGetPrinterId.setText(result.get(RANDOM.nextInt(result.size())).getUuid());
                    Toast.makeText(PrintersListActivity.this,"Got the printer List size="+result.size(), Toast.LENGTH_LONG).show();
                    List<Printer> result_new=new ArrayList<Printer>();

                    for(Printer p:result)
                    {
                        Log.d("Printer category","------>"+p.getCategory()+"=="+Category.ORDER);
                        if(p.getCategory()== Category.ORDER)
                        {
                            result_new.add(p);
                        }
                    }
                    Toast.makeText(PrintersListActivity.this,"Printer Loading completed with size="+result_new.size(), Toast.LENGTH_LONG).show();
                    list.setVisibility(View.VISIBLE);
                    adapter=new PrinterAdapter(PrintersListActivity.this,result_new,(CallBackActivity) PrintersListActivity.this);
                    list.setAdapter(adapter);
                    Loading.setVisibility(View.GONE);

                }
                else
                {
                    list.setVisibility(View.GONE);
                    Loading.setVisibility(View.VISIBLE);
                    Loading.setText(getResources().getString(R.string.OrderAlert_PrinterNotAvailable));
                }
            }

            @Override
            public void onServiceFailure(ResultStatus status) {
                super.onServiceFailure(status);
                //updatePrinters("get printers failure", status, null);
                list.setVisibility(View.GONE);
                Loading.setVisibility(View.VISIBLE);
                Loading.setText(getResources().getString(R.string.OrderAlert_PrinterSorry));
            }

            @Override
            public void onServiceConnectionFailure() {
                super.onServiceConnectionFailure();
              //  updatePrinters("get printers bind failure", null, null);
                list.setVisibility(View.GONE);
                Loading.setVisibility(View.VISIBLE);
                Loading.setText(getResources().getString(R.string.OrderAlert_PrinterSorry));
            }
        });
    }

    private void startAccountChooser() {
        Intent intent = AccountManager.newChooseAccountIntent(null, null, new String[]{CloverAccount.CLOVER_ACCOUNT_TYPE}, false, null, null, null, null);
        startActivityForResult(intent, REQUEST_ACCOUNT)                                                                     ;

    }

    @Override
    protected void onPause() {
        disconnect();
        super.onPause();
    }

    private void connect() {
        disconnect();
        if (account != null) {
            connector = new PrinterConnector(this, account, this);
            connector.connect();

        }
    }

    private void disconnect() {
        if (connector != null) {
            connector.disconnect();
            connector = null;
        }
    }

    @Override
    public void onServiceConnected(ServiceConnector connector) {
        Log.i(TAG, "service connected: " + connector);
        Toast.makeText(this,"Printer list Connected", Toast.LENGTH_LONG).show();
        GetPRinterList();

    }

    @Override
    public void onServiceDisconnected(ServiceConnector connector) {
        Log.i(TAG, "service disconnected: " + connector);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ACCOUNT) {
            if (resultCode == RESULT_OK) {
                String name = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                String type = data.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE);

                account = new Account(name, type);
                if (account != null) {
                    connect();

                }
                Toast.makeText(this,"Account choose", Toast.LENGTH_LONG).show();
               // new queryWebService().execute();
                Log.d("Started","------->execution");
            } else {
                if (account == null) {
                    list.setVisibility(View.GONE);
                    Loading.setVisibility(View.VISIBLE);
                    Loading.setText(getResources().getString(R.string.OrderAlert_AccountNotAvailable));

                }
            }
        }
    }
/*
* Call back function from adapter
*
 */
    @Override
    public void CallBackActivity(int Message) {
        Intent in = getIntent();
        in.putExtra(PrintersListActivity.EXTRA_PRINTER,adapter.getItem(Message));
        setResult(RESULT_OK,in);
       finish();

    }
  /*  class queryWebService extends AsyncTask<Void, String, Void>{


            @Override
            protected Void doInBackground(Void... params) {
                try {
                    //publishProgress("Requesting auth token");
                    CloverAuth.AuthResult authResult = CloverAuth.authenticate(PrintersListActivity.this, account);
                    //publishProgress("Successfully authenticated as " + mAccount + ".  authToken=" + authResult.authToken + ", authData=" + authResult.authData);
                    Log.d("Access tocken","Successfully authenticated as " + account + ".  authToken=" + authResult.authToken + ", authData=" + authResult.authData);
                    if (authResult.authToken != null && authResult.baseUrl != null) {
                        CustomHttpClient httpClient = CustomHttpClient.getHttpClient();
                        String getNameUri = "/v2/merchant/name";
                        String url = authResult.baseUrl + getNameUri + "?access_token=" + authResult.authToken;
                        // publishProgress("requesting merchant id using: " + url);
                        String result = httpClient.get(url);
                        JSONTokener jsonTokener = new JSONTokener(result);
                        JSONObject root = (JSONObject) jsonTokener.nextValue();
                        String merchantId = root.getString("merchantId");
                        //publishProgress("received merchant id: " + merchantId);

                        // now do another get using the merchant id
                        String inventoryUri = "/v2/merchant/" + merchantId + "/inventory/items";
                        url = authResult.baseUrl + inventoryUri + "?access_token=" + authResult.authToken;
Log.d("Access tocken","-------->m="+merchantId+"tocket="+authResult.authToken);

                        //publishProgress("requesting inventory items using: " + url);
                        result = httpClient.get(url);
                        //  publishProgress("received inventory items response: " + result);
                    }
                } catch (Exception e) {
                    // publishProgress("Error retrieving merchant info from server" + e);
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                // mButton.setEnabled(true);
            }
        }*/


}