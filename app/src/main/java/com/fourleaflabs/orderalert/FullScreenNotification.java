package com.fourleaflabs.orderalert;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.TextView;

public class FullScreenNotification extends Activity {

    private Intent manage_order_intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.full_screen_notification_layout);

        Bundle order_data = getIntent().getExtras();
        String order_name = order_data.getString(OrderAlertService.KEY_ORDER_NAME);
        String order_id = order_data.getString(OrderAlertService.KEY_ORDER_ID);
        String order_value = order_data.getString(OrderAlertService.KEY_ORDER_VALUE);
        String order_timestamp = order_data.getString(OrderAlertService.KEY_ORDER_TIMESTAMP);

        // set notification title
        setTitle("New online order for " + order_value);

        // set order summary
        TextView order_summary_view = (TextView) findViewById(R.id.OrderSummaryView);

        order_summary_view.setText("Order " + order_name + " (" + order_id + ") " + " for " + order_value + " was submitted at " + order_timestamp + ".");

        // compose text for option one //
        TextView option_one_view = (TextView) findViewById(R.id.OptionOneView);

        SpannableString option_one = new SpannableString("1. Tap Open to review the order.");

        option_one.setSpan(new StyleSpan(Typeface.BOLD), 7, 11, 0);

        option_one_view.setText(option_one, TextView.BufferType.SPANNABLE);
        /////////////////////////////////

        // compose text for option two //
        TextView option_two_view = (TextView) findViewById(R.id.OptionTwoView);

        SpannableString option_two = new SpannableString("2. After opening the order, tap Reissue Receipt to print the order.");

        option_two.setSpan(new StyleSpan(Typeface.BOLD), 32, 47, 0);

        option_two_view.setText(option_two, TextView.BufferType.SPANNABLE);
        /////////////////////////////////

        // compose intent to open order manager
//        manage_order_intent = new Intent("com.clover.intent.action.START_ORDER_MANAGE");
//        manage_order_intent.putExtra("com.clover.intent.extra.ORDER_ID", order_id);

        // compose intent to open order manager through the service
        manage_order_intent = createIntentToService();
        manage_order_intent.setAction(OrderAlertService.START_ORDER_MANAGE_ACTION);
        manage_order_intent.putExtra(OrderAlertService.KEY_ORDER_ID, order_id);
    }

    public void startManageOrder(View view) {
        startService(manage_order_intent);
        finish();
    }

    public void closeNotification(View view) {

        // compose intent to notify the service to clear the notifications queue
        Intent clear_intent = createIntentToService();
        clear_intent.setAction(OrderAlertService.CLEAR_ORDER_NOTIFICATION_QUEUE);
        startService(clear_intent);

        finish();
    }

    protected Intent createIntentToService()
    {
        return new Intent(this, OrderAlertService.class);
    }
}
