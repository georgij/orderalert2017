package com.fourleaflabs.orderalert;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.clover.sdk.util.CloverAccount;
import com.clover.sdk.v1.ResultStatus;
import com.clover.sdk.v1.ServiceConnector;
import com.clover.sdk.v1.printer.Printer;
import com.clover.sdk.v1.printer.PrinterConnector;
import com.clover.sdk.v3.order.OrderConnector;
import com.fourleaflabs.orderalert.tasks.FirePrintTask;
import com.fourleaflabs.orderalert.tasks.OrderPrintTask;
import com.fourleaflabs.orderalert.tasks.PaymentPrintTask;

import java.util.List;

/**
 * Created by android6 on 24/12/14.
 */
public class ShowOrderDetailActivity extends Activity implements ServiceConnector.OnServiceConnectedListener {

    private static final String TAG = "ShowOrderDetailActivity";
    private Boolean DEBUG = false;

    private String orderID;
    private PrinterConnector connector;
    private Account account;

    private static final int REQUEST_ACCOUNT = 0;

    TextView TV_Top,TV_Printer,Tv_Detail;

    private String order_printer_id;
    private String payment_printer_id;

    private Printer order_printer;
    private Printer payment_printer;

    private TextView message_box;

    private OrderConnector orderConnector;

    String order_name,order_value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        DEBUG = Utils.getBooleanFromManifestMeta(getApplicationContext(), "showDebugLogs", false);

        setContentView(R.layout.activity_order_detail);
        startAccountChooser();
        Bundle order_data = getIntent().getExtras();
         order_name = order_data.getString(OrderAlertService.KEY_ORDER_NAME,"");
         orderID = order_data.getString(OrderAlertService.KEY_ORDER_ID,"");
         order_value = order_data.getString(OrderAlertService.KEY_ORDER_VALUE,"");

        if (DEBUG) Log.d(TAG, "recieved values -------> order details "+orderID+" order_name="+order_name+" order_value="+order_value);

        String order_timestamp = order_data.getString(OrderAlertService.KEY_ORDER_TIMESTAMP);
        if(orderID.equals(""))
        {
            finish();
            if (DEBUG) Log.d(TAG, " Failed to get the order id"+orderID);
            Toast.makeText(this,"Sorry order id empty", Toast.LENGTH_LONG).show();
        }
        TV_Top=(TextView)findViewById(R.id.Order_AlertForText);
        TV_Printer=(TextView)findViewById(R.id.Order_AlertPrinter);
        Tv_Detail=(TextView)findViewById(R.id.Order_AlertDetailtextt);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        order_printer_id = preferences.getString(OrderAlertService.PREFERENCE_KEY_ORDER_RECEIPT_PRINTER_UUID, "");
        payment_printer_id = preferences.getString(OrderAlertService.PREFERENCE_KEY_ORDER_RECEIPT_PRINTER_UUID, "");
        TV_Top.setText("New Online Order For " + order_value);

        Tv_Detail.setText("Order "+order_name+" for "+order_value+" was submited at "+order_timestamp);
        TextView dummy_text1 = ((TextView) findViewById(R.id.OrderAlert_DummyText1));
        dummy_text1.setText(Html.fromHtml("1.Tap <b>Open</b> to review the order."));
        //dummy_text1.setTextAppearance(this, android.R.style.TextAppearance_Large);

        TextView dummy_text2 = ((TextView) findViewById(R.id.OrderAlert_DummyText2));
        dummy_text2.setText(Html.fromHtml("2.Tap <b>Print</b> to print the order receipt and send a confirmation text message."));
       // dummy_text2.setTextAppearance(this, android.R.style.TextAppearance_Large);

        message_box = (TextView) findViewById(R.id.MessageBox);

    }
    private void startAccountChooser() {
        Intent intent = AccountManager.newChooseAccountIntent(null, null, new String[]{CloverAccount.CLOVER_ACCOUNT_TYPE}, false, null, null, null, null);
        startActivityForResult(intent, REQUEST_ACCOUNT);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (account != null) {
            connect();
        }
    }

    @Override
    protected void onPause() {
        if (orderConnector != null) {
            orderConnector.disconnect();
            orderConnector = null;
        }
        disconnect();
        super.onPause();
    }

    private void connect() {
        disconnect();
        if (account != null) {
            connector = new PrinterConnector(this, account, this);
            connector.connect();
        }
    }

    private void disconnect() {
        if (connector != null) {
            connector.disconnect();
            connector = null;
        }
    }

    @Override
    public void onServiceConnected(ServiceConnector connector) {

       if (connector instanceof PrinterConnector)  {
            ((PrinterConnector) connector).getPrinters(new PrinterConnector.PrinterCallback<List<Printer>>() {
                @Override
                public void onServiceSuccess(List<Printer> result, ResultStatus status) {
                    if (!result.isEmpty()) {
                        for (Printer printer : result) {
                            if (printer.getUuid().equals(order_printer_id)) {
                                order_printer = printer;
                            }
                            if (printer.getUuid().equals(payment_printer_id)) {
                                payment_printer = printer;
                            }
                        }
                    }
                }

                @Override
                public void onServiceFailure(ResultStatus status) {
                    Toast.makeText(ShowOrderDetailActivity.this, "Failed to get printe(s) [" + status.toString() + "]", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onServiceConnectionFailure() {
                    Toast.makeText(ShowOrderDetailActivity.this, "Failed to connect to order_printer(s)", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    @Override
    public void onServiceDisconnected(ServiceConnector connector) {
        if (DEBUG) Log.i(TAG, "service disconnected: " + connector);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ACCOUNT) {
            if (resultCode == RESULT_OK) {
                String name = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                String type = data.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE);
                account = new Account(name, type);
                orderConnector = new OrderConnector(getApplicationContext(), account, null);

            } else {
                if (account == null) {
                    Toast.makeText(this, "Sorry we cannot find any account", Toast.LENGTH_LONG).show();

                }
            }
        }
    }

    public void CloseClick(View v)
    {
        finish();
    }

    public void OpenClick(View v)
    {

        Intent manage_order_intent = createIntentToService();
        manage_order_intent.setAction(OrderAlertService.START_ORDER_MANAGE_ACTION);
        manage_order_intent.putExtra(OrderAlertService.KEY_ORDER_ID, orderID);
        startService(manage_order_intent);
        Toast.makeText(this, "Opening ....", Toast.LENGTH_LONG).show();
    }

    public void doPrintByFire(View view) {
        new FirePrintTask(orderID, orderConnector).execute();
    }

    public void doPrintOrder(View view) {
        if (order_printer != null && !orderID.equals("")) {
            Toast.makeText(this, "Printing order " + order_name + " to " + order_printer.name + " (" + order_printer.getCategory() + ")", Toast.LENGTH_LONG).show();
            new OrderPrintTask(getApplicationContext(), order_printer, orderID, orderConnector).execute();
        } else {
            Toast.makeText(this, "No printer or no order id.", Toast.LENGTH_SHORT).show();
        }
    }

    public void doPrintPayment(View view) {
        if (payment_printer != null && !orderID.equals("")) {
            Toast.makeText(this, "Printing payment for " + order_name + " to " + payment_printer.name + " (" + payment_printer.getCategory() + ")", Toast.LENGTH_SHORT).show();
            new PaymentPrintTask(getApplicationContext(), payment_printer, orderID, orderConnector).execute();
        } else {
            Toast.makeText(this, "No printer or no order id.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    protected Intent createIntentToService() {
        return new Intent(ShowOrderDetailActivity.this, OrderAlertService.class);
    }
}
