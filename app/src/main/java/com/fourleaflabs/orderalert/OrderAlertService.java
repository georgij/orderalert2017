package com.fourleaflabs.orderalert;

import android.accounts.Account;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.preference.PreferenceManager;
import android.util.Log;

import com.clover.sdk.util.CloverAccount;
import com.clover.sdk.util.CloverAuth;
import com.clover.sdk.v1.ServiceConnector;
import com.clover.sdk.v1.app.AppNotification;
import com.clover.sdk.v1.app.AppNotificationReceiver;
import com.clover.sdk.v1.printer.Printer;
import com.clover.sdk.v1.printer.PrinterConnector;
import com.clover.sdk.v3.order.OrderConnector;
import com.crashlytics.android.Crashlytics;
import com.fourleaflabs.orderalert.tasks.GetCloverAuth;
import com.fourleaflabs.orderalert.tasks.NotifyServerTask;
import com.fourleaflabs.orderalert.tasks.OrderNoteUpdateTask;
import com.fourleaflabs.orderalert.tasks.OrderPrintTask;
import com.fourleaflabs.orderalert.tasks.PaymentPrintTask;
import com.fourleaflabs.orderalert.tasks.UploadCurrentTokenTask;
import com.logentries.logger.AndroidLogger;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.PNCallback;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.enums.PNReconnectionPolicy;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;

import java.io.IOException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Currency;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.TimeZone;
import java.util.regex.Pattern;

import io.fabric.sdk.android.Fabric;

public class OrderAlertService extends Service implements SharedPreferences.OnSharedPreferenceChangeListener, ServiceConnector.OnServiceConnectedListener {

    protected final String TAG = "OrderAlertService";
    protected final String TAG_PUBNUB = "PubNub";
    protected boolean DEBUG = false;

    protected final String NEW_ONLINE_ORDER_EVENT = "new_online_order";
    public static final String START_ORDER_MANAGE_ACTION = "start_manage_order";
    public static final String CLEAR_ORDER_NOTIFICATION_QUEUE = "clear_notification_queue";

    private final int NOTIFICATION_ID = 234623;
    private final int FULL_SCREEN_NOTIFICATION_ID = 234624;
    private final int PENDING_REGISTER_INTENT_REQUEST_CODE = 943751;
    private final int PENDING_REGISTER_FULL_SCREEN_INTENT_REQUEST_CODE = 943752;
    private final String UUID_KEY = "DeviceUUID";

    protected final int ORDER_TITLE = 0;
    protected final int ORDER_TIMESTAMP = 1;
    protected final int ORDER_AMOUNT = 2;
    protected final int ORDER_CURRENCY = 3;
    protected final int ORDER_ID = 4;

    // Setting Keys
    public final static String PREFERENCE_KEY_ENABLE_ORDER_ALERTS = "KeyEnableOrderAlerts";
    public final static String PREFERENCE_KEY_FULL_SCREEN_NOTIFICATION = "KeyFullScreenNotification";
    public final static String PREFERENCE_KEY_ACCEPT_ORDERS = "KeyAcceptOrders";
    public final static String PREFERENCE_KEY_FIRE_AUTO_PRINT = "KeyFireAutoPrint";
    public final static String PREFERENCE_KEY_ORDER_RECEIPT_AUTO_PRINT = "KeyOrderReceiptAutoPrint";
    public final static String PREFERENCE_KEY_ORDER_RECEIPT_PRINTER_NAME = "KeyOrderReceiptPrinterName";
    public final static String PREFERENCE_KEY_ORDER_RECEIPT_PRINTER_UUID = "KeyOrderReceiptPrinterUUID";
    public final static String PREFERENCE_KEY_PAYMENT_RECEIPT_AUTO_PRINT = "KeyPaymentReceiptAutoPrint";
    public final static String PREFERENCE_KEY_PAYMENT_RECEIPT_PRINTER_NAME = "KeyPaymentReceiptPrinterName";
    public final static String PREFERENCE_KEY_PAYMENT_RECEIPT_PRINTER_UUID = "KeyPaymentReceiptPrinterUUID";

    public static String KEY_ORDER_NAME = "com.fourleaflabs.orderalert.core.OrderName";
    public static String KEY_ORDER_ID = "com.clover.intent.extra.ORDER_ID";
    public static String KEY_ORDER_VALUE = "com.fourleaflabs.orderalert.core.OrderValue";
    public static String KEY_ORDER_TIMESTAMP = "com.fourleaflabs.orderalert.core.OrderTimestamp";

    private NotificationManager notification_manager;
    protected Pattern payload_split_pattern;
    private Bitmap large_icon;

    private boolean use_full_screen_notification;

    /**
     * Flag to use fire to print.
     */
    private boolean print_using_fire_automatically;

    /**
     * Flag to autoprint order receipt.
     */
    private boolean autoprint_order_receipts;
    /**
     * ID of order receipt printer.
     */
    private String order_receipt_printer_id;

    /**
     * Flag to autoprint payment receipt.
     */
    private boolean autoprint_payment_receipts;
    /**
     * ID of payment receipt printer.
     */
    private String payment_receipt_printer_id;

    private OrderConnector orderConnector;

    protected LinkedList<OrderNotification> order_notification_queue;

    /**
     * Set of received order ids. Used to check for duplicates.
     */
    protected HashSet<String> received_order_ids;

    // printer related entities //
    private PrinterConnector connector;

    private PNConfiguration pnConfiguration;
    private PubNub pubNub;

    private String channelSubscribe = "merchantid-order";
    private Account account;
    private CloverAuth.AuthResult cloverAuth;
    private String token = "";
    private SubscribeCallback subscribeCallback;
    private PNCallback pnCallback;

    private Handler handler;
    private Runnable connectionRunable;
    private Boolean pubNubConnected = false;
    protected AndroidLogger leLogger;


    //TODO: write another class for this
    private void startPubNub() throws OperationCanceledException, AuthenticatorException, IOException {

        try {

            cloverAuth = new GetCloverAuth(getApplicationContext()).execute().get();
            if (cloverAuth != null && cloverAuth.authToken != null) {
                channelSubscribe = cloverAuth.merchantId + '-' + "order";
                if (DEBUG) Log.i(TAG, "Channel subscription: " + channelSubscribe);
                leLogger.log("Channel subscription: " + channelSubscribe);
            } else {
                if (DEBUG) Log.e(TAG, "Cannot get Clover authentication");
            }

            if (pubNub == null) {

                pnConfiguration = new PNConfiguration();
                pnConfiguration.setPublishKey(BuildConfig.PubNubPublishKey);
                pnConfiguration.setSubscribeKey(BuildConfig.PubNubSubscribeKey);
                pnConfiguration.setReconnectionPolicy(PNReconnectionPolicy.LINEAR);

                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
                String uuid = preferences.getString(UUID_KEY, "");
                if (uuid == null || uuid.equals("")) {
                    uuid = java.util.UUID.randomUUID().toString();
                    Utils.updateSetting(this, UUID_KEY, uuid);
                    if (DEBUG) Log.i(TAG, "UUID set: " + uuid);
                }
                if (DEBUG) Log.i(TAG, "Current UUID: " + uuid);
                leLogger.log("Current UUID: " + uuid);
                
                pnConfiguration.setUuid(uuid);

                pubNub = new PubNub(pnConfiguration);

                subscribePnListener();

                pubNub.subscribe().channels(Arrays.asList(channelSubscribe)).execute();

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void subscribePnListener() {

        subscribeCallback = new SubscribeCallback() {
            @Override
            public void status(PubNub p, PNStatus status) {

                switch (status.getCategory()) {
                    case PNConnectedCategory:
                        if (DEBUG) Log.i(TAG_PUBNUB,"Pubnub Connected");
                        break;
                    case PNReconnectedCategory:
                        if (DEBUG) Log.i(TAG_PUBNUB,"Pubnub reconnected");
                        break;
                    case PNDisconnectedCategory:
                        if (DEBUG) Log.w(TAG_PUBNUB,"Pubnub disconnected");
                        break;
                    case PNUnexpectedDisconnectCategory:
                        if (DEBUG) Log.w(TAG_PUBNUB,"Pubnub Unexcpected disconnect");
                        pubNub.reconnect();
                        break;
                    case PNTimeoutCategory:
                        if (DEBUG) Log.w(TAG_PUBNUB,"Pubnub Timeout");
                        pubNub.reconnect();
                        break;
                    default:
                        if (DEBUG) Log.i(TAG_PUBNUB,"Pubnub " + status.getCategory().name());
                        break;
                }
            }

            @Override
            public void message(PubNub pubnub, PNMessageResult message) {
                ServerMessage serverMessage = new ServerMessage(message.getMessage().toString());
                if (serverMessage.getType().equals(NEW_ONLINE_ORDER_EVENT)) {
                    try {
                        if (DEBUG) Log.i(TAG, "Notification received: " + serverMessage.getAsString());
                        leLogger.log("Notification received: " + serverMessage.getAsString() + " -- Merchant ID: " + cloverAuth.merchantId);
                        new NotifyServerTask(getApplicationContext(), serverMessage).execute();
                        new OrderNoteUpdateTask(getApplicationContext(), cloverAuth, serverMessage.getOrderId()).execute();
                        handlePrintAndNotify(serverMessage);
                    } catch (Error e) {
                        leLogger.log("Pubnub message error " + e.getMessage());
                        if (DEBUG) Log.e(TAG, e.getMessage());
                    } catch (IOException e) {
                        leLogger.log("Pubnub message IOException " + e.getMessage());
                        e.printStackTrace();
                    }
                } else {
                    if (DEBUG) Log.w(TAG, "Do not print " + serverMessage.getAsString());
                    leLogger.log("Do not print " + serverMessage.getAsString());
                }
            }

            @Override
            public void presence(PubNub pubnub, PNPresenceEventResult presence) {
            }
        };

        pubNub.addListener(subscribeCallback);
    }

    private void unsubscribePnListener() {
        subscribeCallback = new SubscribeCallback() {
            @Override
            public void status(PubNub pubnub, PNStatus status) {
            }

            @Override
            public void message(PubNub pubnub, PNMessageResult message) {
            }

            @Override
            public void presence(PubNub pubnub, PNPresenceEventResult presence) {
            }
        };

        pubNub.addListener(subscribeCallback);
        pubNub.removeListener(subscribeCallback);
        subscribeCallback = null;
    }

    private void stopPubNub() {

        pubNubConnected = false;
        unsubscribePnListener();
        pubNub.disconnect();
        pubNub = null;

        if (DEBUG) Log.i(TAG_PUBNUB, "PubNub stopped");
    }

    private void handlePrintAndNotify(ServerMessage serverMessage) {

        String order_title = serverMessage.getOrderTitle();

        long order_timestamp = 0;
        try {
            order_timestamp = Long.parseLong(serverMessage.getCreatedTime());
        } catch (NumberFormatException e) {
            if (DEBUG) Log.e(TAG, "Invalid timestamp - " + e.getMessage());
        }

        int order_amount = 0;
        try {
            order_amount = Integer.parseInt(serverMessage.getOrderTotal());
        } catch (NumberFormatException e) {
            if (DEBUG) Log.e(TAG, "Invalid amount - " + e.getMessage());
        }

        String order_currency = serverMessage.getCurrency();

        String order_id = serverMessage.getOrderId();

        if (!received_order_ids.contains(order_id)) {

            if (DEBUG) Log.i(TAG, "New notification " + order_id);

            OrderNotification order_notification = new OrderNotification(order_title, order_timestamp, order_amount, order_currency, order_id);

            order_notification_queue.add(order_notification);

            received_order_ids.add(order_id);

            postNotification();

            printIfNecessary(order_id);
        } else {
            if (DEBUG) Log.i(TAG, "Found duplicate: " + order_id);
        }
    }

    /**
     * Receives an app notification and logs it.
     */
    private AppNotificationReceiver receiver = new AppNotificationReceiver() {
        @Override
        public synchronized void onReceive(Context context, AppNotification notification) {
            // not used at the moment
        }
    };

    @Override
    public void onCreate() {

        super.onCreate();

        Fabric.with(this, new Crashlytics());

        account = CloverAccount.getAccount(getApplicationContext());
        orderConnector = new OrderConnector(getApplicationContext(), account, null);

        try {
            leLogger = AndroidLogger.createInstance(getApplicationContext(), false, true, false, null, 0, BuildConfig.LogentriesKey, true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        DEBUG = Utils.getBooleanFromManifestMeta(getApplicationContext(), "showDebugLogs", false);

        if (DEBUG) Log.i(TAG, "Started! (" + this.toString() + ")");

        // setup printing service
        setupPrinterConnection();

        notification_manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        use_full_screen_notification = preferences.getBoolean(PREFERENCE_KEY_FULL_SCREEN_NOTIFICATION,
                getResources().getBoolean(R.bool.FullScreenNotificationDefaultValue));

        print_using_fire_automatically = preferences.getBoolean(PREFERENCE_KEY_FIRE_AUTO_PRINT, false);

        autoprint_order_receipts = preferences.getBoolean(PREFERENCE_KEY_ORDER_RECEIPT_AUTO_PRINT, false);
        order_receipt_printer_id = preferences.getString(OrderAlertService.PREFERENCE_KEY_ORDER_RECEIPT_PRINTER_UUID, "");

        autoprint_payment_receipts = preferences.getBoolean(PREFERENCE_KEY_PAYMENT_RECEIPT_AUTO_PRINT, false);
        payment_receipt_printer_id = preferences.getString(OrderAlertService.PREFERENCE_KEY_PAYMENT_RECEIPT_PRINTER_UUID, "");

        if (DEBUG) Log.i(TAG, "Order Printer ID: >" + order_receipt_printer_id + "<, Payment Printer ID: >" + payment_receipt_printer_id + "<");

        preferences.registerOnSharedPreferenceChangeListener(this);

        Bitmap large_source_image = BitmapFactory.decodeResource(getResources(), R.drawable.notification_large_icon);

        Resources resources = getResources();
        int new_width = resources.getDimensionPixelSize(android.R.dimen.notification_large_icon_width);
        int new_height = resources.getDimensionPixelSize(android.R.dimen.notification_large_icon_height);

        large_icon = Bitmap.createScaledBitmap(large_source_image, new_width, new_height, true);

        order_notification_queue = new LinkedList<>();

        received_order_ids = new HashSet<>();

        payload_split_pattern = Pattern.compile("\\|");

        try {
            startPubNub();
        } catch (Exception e) {
            e.printStackTrace();
        }

        receiver.register(this);
    }

    @Override
    public synchronized int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null) {
            if (intent.getAction() == START_ORDER_MANAGE_ACTION) {

                /*String order_id = intent.getExtras().getString("com.clover.intent.extra.ORDER_ID");
                order_notification_queue.remove(order_id);*/

                order_notification_queue.clear();
                notification_manager.cancel(NOTIFICATION_ID);

                Intent manage_order_intent = new Intent("com.clover.intent.action.START_ORDER_MANAGE");
                manage_order_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                manage_order_intent.putExtras(intent.getExtras());

                startActivity(manage_order_intent);
            } else if (intent.getAction() == CLEAR_ORDER_NOTIFICATION_QUEUE) {

                order_notification_queue.clear();
                notification_manager.cancel(NOTIFICATION_ID);
            }
        }

        new UploadCurrentTokenTask(getApplicationContext()).execute();

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // we don't use binding for now //
        return null;
    }

    protected synchronized void postNotification() {

        OrderNotification oldest_order = order_notification_queue.getFirst();
        OrderNotification latest_order = order_notification_queue.getLast();

        int orders_count = order_notification_queue.size();

        // create intent which returns to this service
        Intent start_register_intent = new Intent(this, OrderAlertService.class);
        start_register_intent.setAction(START_ORDER_MANAGE_ACTION);
        start_register_intent.putExtra(OrderAlertService.KEY_ORDER_ID, oldest_order.order_id);

        PendingIntent pending_register_intent = PendingIntent.getService(this, PENDING_REGISTER_INTENT_REQUEST_CODE, start_register_intent, PendingIntent.FLAG_ONE_SHOT);

        Notification.Builder notification_builder = new Notification.Builder(this)
                .setContentIntent(pending_register_intent)
                .setContentTitle(orders_count + " new order" + ((orders_count > 1) ? "s" : ""))
                .setContentText("Last: " + latest_order.order_timestamp + " " + latest_order.order_title + " " + latest_order.order_value)
                .setSmallIcon(R.drawable.notification_small_icon)
                .setLargeIcon(large_icon)
                .setAutoCancel(true);

        // show additional full screen notification if settings requires so
        //Log.d("Comes in order alert core","-------> works prefrence "+use_full_screen_notification);
        if (use_full_screen_notification) {

            Intent full_screen_notification_intent = new Intent("com.fourleaflabs.orderalert.notification.SHOW_FULL_SCREEN_NOTIFICATION");
            full_screen_notification_intent.putExtra(KEY_ORDER_ID, latest_order.order_id);
            full_screen_notification_intent.putExtra(KEY_ORDER_NAME, latest_order.order_title);
            full_screen_notification_intent.putExtra(KEY_ORDER_VALUE, latest_order.order_value);
            full_screen_notification_intent.putExtra(KEY_ORDER_TIMESTAMP, latest_order.order_timestamp);

//            PendingIntent pending_full_screen_notification_intent = PendingIntent.getActivity(this, PENDING_REGISTER_FULL_SCREEN_INTENT_REQUEST_CODE, full_screen_notification_intent, PendingIntent.FLAG_CANCEL_CURRENT);
//
//            // notification_builder.setPriority(Notification.PRIORITY_MAX)
//            notification_builder.setFullScreenIntent(pending_full_screen_notification_intent, true);

            full_screen_notification_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            startActivity(full_screen_notification_intent);
            //Log.d("Comes in order alert core","-------> works fine here ");

        } else {
            notification_builder.setTicker("New online order " + latest_order.order_title + " for " + latest_order.order_value);
        }

        // compose big style view for this notification //
        Notification.InboxStyle inbox_style = new Notification.InboxStyle();
        inbox_style.setBigContentTitle("All orders:");

        for (int i = 0; i < order_notification_queue.size(); i++) {

            OrderNotification order_notification = order_notification_queue.get(i);

            inbox_style.addLine(order_notification.order_timestamp + " " + order_notification.order_title + " " + order_notification.order_value);
        }

        notification_builder.setStyle(inbox_style);
        //////////////////////////////////////////////////

        Notification notification = notification_builder.build();

        notification.defaults |= Notification.DEFAULT_SOUND;

        notification_manager.notify(NOTIFICATION_ID, notification);
    }

    /// auto print ///

    private void setupPrinterConnection() {
        Account account = CloverAccount.getAccount(getApplicationContext());
        connector = new PrinterConnector(this, account, this);
    }

    @Override
    public void onServiceConnected(ServiceConnector<? extends IInterface> connector) {
        if (DEBUG) Log.i(TAG, "Printer service connected.");
    }

    protected void printIfNecessary(final String order_id) {

        if (print_using_fire_automatically) {
            try {
                orderConnector.fire(order_id);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (autoprint_order_receipts || autoprint_payment_receipts) {

            if (autoprint_order_receipts && order_receipt_printer_id.length() > 0) {
                try {
                    Printer order_printer = connector.getPrinter(order_receipt_printer_id);
                    if (order_printer != null) {
                        if (DEBUG) Log.i(TAG, "Starting order print job for order " + order_id + " on printer " + order_printer.getUuid());
                        new OrderPrintTask(getApplicationContext(), order_printer, order_id, orderConnector).execute();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Problem getting printer [" + order_receipt_printer_id + "] - " + e.getMessage());
                }
            }

            if (autoprint_payment_receipts && payment_receipt_printer_id.length() > 0) {
                try {
                    Printer payment_printer = connector.getPrinter(payment_receipt_printer_id);
                    if (payment_printer != null) {
                        if (DEBUG) Log.i(TAG, "Starting payment print job for order " + order_id + " on printer " + payment_printer.getUuid());
                        new PaymentPrintTask(getApplicationContext(), payment_printer, order_id, orderConnector).execute();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Problem getting printer [" + order_receipt_printer_id + "] - " + e.getMessage());
                }
            }
        }
    }

    @Override
    public void onServiceDisconnected(ServiceConnector<? extends IInterface> connector) {
        if (DEBUG) Log.i(TAG, "Printer service disconnected.");
    }

    //////////////////

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

        if (key.equals(PREFERENCE_KEY_FULL_SCREEN_NOTIFICATION)) {
            use_full_screen_notification = sharedPreferences.getBoolean(PREFERENCE_KEY_FULL_SCREEN_NOTIFICATION,
                    getResources().getBoolean(R.bool.FullScreenNotificationDefaultValue));
        } else if (key.equals(PREFERENCE_KEY_FIRE_AUTO_PRINT)) {
            print_using_fire_automatically = sharedPreferences.getBoolean(PREFERENCE_KEY_FIRE_AUTO_PRINT,
                    getResources().getBoolean(R.bool.PrintUsingFireAutomaticallyDefaultValue));
        } else if (key.equals(PREFERENCE_KEY_ORDER_RECEIPT_AUTO_PRINT)) {
            autoprint_order_receipts = sharedPreferences.getBoolean(PREFERENCE_KEY_ORDER_RECEIPT_AUTO_PRINT,
                    getResources().getBoolean(R.bool.PrintOrderReceiptAutomaticallyDefaultValue));
        } else if (key.equals(PREFERENCE_KEY_ORDER_RECEIPT_PRINTER_UUID)) {
            order_receipt_printer_id = sharedPreferences.getString(PREFERENCE_KEY_ORDER_RECEIPT_PRINTER_UUID, "");
        } else if (key.equals(PREFERENCE_KEY_PAYMENT_RECEIPT_AUTO_PRINT)) {
            autoprint_payment_receipts = sharedPreferences.getBoolean(PREFERENCE_KEY_PAYMENT_RECEIPT_AUTO_PRINT,
                    getResources().getBoolean(R.bool.PrintPaymentReceiptAutomaticallyDefaultValue));
        } else if (key.equals(PREFERENCE_KEY_PAYMENT_RECEIPT_PRINTER_UUID)) {
            payment_receipt_printer_id = sharedPreferences.getString(PREFERENCE_KEY_PAYMENT_RECEIPT_PRINTER_UUID, "");
        }

    }

    @Override
    public void onDestroy() {

        stopPubNub();

        leLogger = null;

        receiver.unregister();

        notification_manager.cancelAll();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        preferences.unregisterOnSharedPreferenceChangeListener(this);

        if (orderConnector != null) {
            orderConnector.disconnect();
            orderConnector = null;
        }

        if (DEBUG) Log.i(TAG, "Destroyed!");

        super.onDestroy();
    }

    private class OrderNotification {

        final String order_title;
        final String order_timestamp;
        final String order_value;
        final String order_id;

        OrderNotification(String order_title, long order_timestamp, int order_amount, String order_currency, String order_id) {

            this.order_title = order_title;

            Date order_timestamp_date = new Date(order_timestamp);
            DateFormat time_formatter = DateFormat.getTimeInstance(DateFormat.SHORT);
            time_formatter.setTimeZone(TimeZone.getDefault());
            this.order_timestamp = time_formatter.format(order_timestamp_date);

            Currency amount_currency = Currency.getInstance(order_currency);
            NumberFormat amount_formatter = NumberFormat.getCurrencyInstance();
            amount_formatter.setCurrency(amount_currency);

            float factor = 1.0f;
            for (int i = amount_currency.getDefaultFractionDigits(); i > 0; i--) {
                factor *= 10;
            }

            // this.order_value = amount_formatter.format(order_amount * FloatMath.pow(10, -amount_currency.getDefaultFractionDigits()));

            this.order_value = amount_formatter.format(order_amount / factor);

            this.order_id = order_id;
        }

    }
}
