package com.fourleaflabs.orderalert.tasks;

import android.accounts.Account;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.clover.sdk.util.CloverAccount;
import com.clover.sdk.util.CloverAuth;
import com.fourleaflabs.orderalert.BuildConfig;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by georgsisow on 21.11.15.
 */
public class UploadCurrentTokenTask extends AsyncTask<Void, Void, Boolean> {

    private static final Boolean DEBUG = false;
    private static final String TAG = UploadCurrentTokenTask.class.getName();

    private Context context;

    public UploadCurrentTokenTask(Context context) {
        this.context = context;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        boolean success = false;

        Account account = CloverAccount.getAccount(context);

        if (account != null) {

            HttpURLConnection upload_connection = null;

            try {

                CloverAuth.AuthResult auth_result = CloverAuth.authenticate(context, account);

                if (DEBUG) Log.i(TAG, "Auth token is " + auth_result.authToken);

                URL upload_token_url = new URL(BuildConfig.OATokenUploadURL +
                    "?merchantId=" + auth_result.merchantId + "&accessToken=" + auth_result.authToken);

                upload_connection = (HttpURLConnection) upload_token_url.openConnection();

                if (upload_connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    if (DEBUG) Log.i(TAG, "Token uploaded");
                    success = true;
                }
                else {
                    if (DEBUG) Log.w(TAG, "Upload failed - " + upload_connection.getResponseCode() + " / " + upload_connection.getResponseMessage());
                }
            }
            catch (OperationCanceledException e) {
                if (DEBUG) Log.e(TAG, "Problem occurred - " + e.getMessage());
            }
            catch (AuthenticatorException e) {
                if (DEBUG) Log.e(TAG, "Problem occurred - " + e.getMessage());
            }
            catch (IOException e) {
                if (DEBUG) Log.e(TAG, "Problem occurred - " + e.getMessage());
            }
            finally {
                if (upload_connection != null)
                    upload_connection.disconnect();
            }
        }
        else {
            if (DEBUG) Log.w(TAG, "Failed to obtain Clover Account!");
        }

        return success;
    }

}
