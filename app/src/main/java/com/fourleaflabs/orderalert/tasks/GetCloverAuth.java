package com.fourleaflabs.orderalert.tasks;

import android.accounts.Account;
import android.accounts.OperationCanceledException;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.clover.sdk.util.CloverAccount;
import com.clover.sdk.util.CloverAuth;


public class GetCloverAuth extends AsyncTask<Void, Void, CloverAuth.AuthResult> {

    private final String TAG = "GetCloverAuth";
    private Context context;
    private Account account;

    public GetCloverAuth(Context context) {
        this.context = context;
    }

    @Override
    protected CloverAuth.AuthResult doInBackground(Void... params) {
        try {
            if (account == null) {
                account = CloverAccount.getAccount(context);
            }
            return CloverAuth.authenticate(context, account);
        } catch (OperationCanceledException e) {
            Log.e(TAG, "Authentication cancelled", e);
        } catch (Exception e) {
            Log.e(TAG, "Error retrieving authentication", e);
        }
        return null;
    }
}
