package com.fourleaflabs.orderalert.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fourleaflabs.orderalert.BuildConfig;
import com.fourleaflabs.orderalert.ServerMessage;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class NotifyServerTask extends AsyncTask<Void, Void, Void> {

    private static final String TAG = NotifyServerTask.class.getName();

    private Context context;
    private ServerMessage data;

    public NotifyServerTask(Context context, ServerMessage data) throws IOException {
        this.context = context;
        this.data = data;
    }

    @Override
    protected Void doInBackground(Void... voids) {

        String url = BuildConfig.ApiUrl + "/notificationReceived";
        final ServerMessage data = this.data;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i(TAG, "Notification callback succeed");
            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.w(TAG, "Failed to send notification callback");
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> postData = new HashMap<String, String>();
                postData.put("orderId", data.getOrderId());
                postData.put("data", data.getAsString());
                return postData;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this.context);
        requestQueue.add(stringRequest);

        return null;
    }

}
