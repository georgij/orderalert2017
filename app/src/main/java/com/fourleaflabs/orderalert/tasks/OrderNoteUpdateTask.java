package com.fourleaflabs.orderalert.tasks;

import android.accounts.Account;
import android.content.Context;
import android.os.AsyncTask;
import android.os.RemoteException;
import android.util.Log;

import com.clover.sdk.util.CloverAccount;
import com.clover.sdk.util.CloverAuth;
import com.clover.sdk.v1.BindingException;
import com.clover.sdk.v1.ClientException;
import com.clover.sdk.v1.ServiceException;
import com.clover.sdk.v3.order.Order;
import com.clover.sdk.v3.order.OrderConnector;
import com.fourleaflabs.orderalert.BuildConfig;
import com.fourleaflabs.orderalert.R;
import com.fourleaflabs.orderalert.Utils;

import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class OrderNoteUpdateTask extends AsyncTask<Void, Void, Void> {

    private final String TAG = NotifyServerTask.class.getName();

    private Context context;
    private String orderId;
    private Account cloverAccount;
    private OrderConnector orderConnector;
    private Order order;
    private CloverAuth.AuthResult cloverAuth;

    public OrderNoteUpdateTask(Context context, CloverAuth.AuthResult cloverAuth, String orderId) {
        this.context = context;
        this.orderId = orderId;
        this.cloverAuth = cloverAuth;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        cloverAccount = CloverAccount.getAccount(this.context);
        orderConnector = new OrderConnector(this.context, cloverAccount, null);
    }

    @Override
    protected Void doInBackground(Void... voids) {

        String url = BuildConfig.CloverApiUrl + "/v3/merchants/" +
                this.cloverAuth.merchantId + "/orders/" + this.orderId + "?access_token=" + this.cloverAuth.authToken;
        try {
            String orderNote = "#order-confirmed " + getTimeString() + " " + getOrderNote();
            Utils.CustomHttpClient client = Utils.CustomHttpClient.getHttpClient();
            JSONObject request = new JSONObject();
            request.put("note", orderNote);

            Log.i(TAG, "Updating order note " + this.orderId);
            client.post(url, request.toString());
        } catch (Exception e) {
            Log.w(TAG, "Failed on updating order note");
        }

        return null;
    }

    private String getOrderNote() throws RemoteException, ServiceException, BindingException, ClientException {
        order = orderConnector.getOrder(this.orderId);
        return order.getNote();
    }

    public String getTimeString() {
        SimpleDateFormat date_time_formatter = new SimpleDateFormat("yyyy-MM-dd K:mm a z", Locale.getDefault());
        date_time_formatter.setTimeZone(TimeZone.getDefault());
        return date_time_formatter.format(Calendar.getInstance().getTime());
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        orderConnector.disconnect();
        orderConnector = null;
        super.onPostExecute(aVoid);
    }
}
