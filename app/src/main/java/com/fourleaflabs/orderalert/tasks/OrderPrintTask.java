package com.fourleaflabs.orderalert.tasks;

import android.accounts.Account;
import android.content.Context;
import android.os.AsyncTask;

import com.clover.sdk.util.CloverAccount;
import com.clover.sdk.v1.printer.Printer;
import com.clover.sdk.v1.printer.job.PrintJob;
import com.clover.sdk.v1.printer.job.StaticOrderPrintJob;
import com.clover.sdk.v3.order.Order;
import com.clover.sdk.v3.order.OrderConnector;

/**
 * Created by Georg Sisow on 17.10.17.
 */
public class OrderPrintTask extends AsyncTask<Void, Void, Boolean>  {

    private Context context;
    private Printer printer;
    private String orderId;
    private OrderConnector orderConnector;

    public OrderPrintTask(Context context, Printer printer, String orderId, OrderConnector orderConnector) {
        this.context = context;
        this.printer = printer;
        this.orderId = orderId;
        this.orderConnector = orderConnector;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        Account account = CloverAccount.getAccount(context);

        Order order;

        try {
            order = orderConnector.getOrder(orderId);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        PrintJob print_job = new StaticOrderPrintJob.Builder().order(order).build();
        print_job.print(context, account, printer);

        return true;
    }
}