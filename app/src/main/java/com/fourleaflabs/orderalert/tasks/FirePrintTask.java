package com.fourleaflabs.orderalert.tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.clover.sdk.v3.order.OrderConnector;

/**
 * Created by Georg Sisow on 17.10.17.
 */

public class FirePrintTask extends AsyncTask<Void, Void, Boolean> {

    private final String TAG = FirePrintTask.class.getName();

    private String orderID;
    private OrderConnector orderConnector;

    public FirePrintTask(String orderID, OrderConnector orderConnector) {
        this.orderID = orderID;
        this.orderConnector = orderConnector;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        try {
            orderConnector.fire(orderID);
        } catch (Exception e) {
            Log.e(TAG, "Problem printing using fire occurred: " + e.getMessage());
        }

        return null;
    }
}
