package com.fourleaflabs.orderalert;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
//import android.webkit.WebView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.clover.sdk.v1.printer.Printer;
import com.fourleaflabs.orderalert.broadcastreceivers.TokenUpdateAlarmSetupReceiver;
import com.fourleaflabs.orderalert.interfaces.RequestCallback;

/**
 * Created by georgsisow on 24.11.15.
 */
public class OrderAlert extends Activity implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    private final String TAG = OrderAlert.class.getName();

    final int REQUEST_ORDER_RECEIPT_PRINTER = 5265;
    final int REQUEST_PAYMENT_RECEIPT_PRINTER = 5266;

    private Switch order_alert_active_option;
    private Switch full_screen_notification_option;
    private Switch accept_order_option;
    private Switch print_using_fire_automatically_option;

    private Switch print_order_receipt_automatically_option;
    private TextView selected_order_receipt_printer_view;

    private Switch print_payment_receipt_automatically_option;
    private TextView selected_payment_receipt_printer_view;

    private Intent order_alert_service_intent;

    protected Button dashboard_button;
    protected CloverMerchant cloverMerchant;
//    protected WebView dashboard_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.order_alert_layout);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        order_alert_active_option = findViewById(R.id.OrderAlertOption);
        full_screen_notification_option = findViewById(R.id.FullScreenNotificationOption);
        accept_order_option = findViewById(R.id.AcceptOrdersOption);
        print_using_fire_automatically_option = findViewById(R.id.PrintUsingFireAutomaticallyOption);

        print_order_receipt_automatically_option = findViewById(R.id.OrderAutoPrintOption);
        selected_order_receipt_printer_view = findViewById(R.id.SelectedOrderPrinterView);

        print_payment_receipt_automatically_option = findViewById(R.id.PaymentAutoPrintOption);
        selected_payment_receipt_printer_view = findViewById(R.id.SelectedPaymentPrinterView);

        dashboard_button = findViewById(R.id.DashboardButton);

        // setup states according to saved values

        boolean service_is_running = isMyServiceRunning(OrderAlertService.class);
        order_alert_active_option.setChecked(service_is_running);

        full_screen_notification_option.setChecked(preferences.getBoolean(OrderAlertService.PREFERENCE_KEY_FULL_SCREEN_NOTIFICATION,
                getResources().getBoolean(R.bool.FullScreenNotificationDefaultValue)));

        accept_order_option.setChecked(preferences.getBoolean(OrderAlertService.PREFERENCE_KEY_ACCEPT_ORDERS,
                getResources().getBoolean(R.bool.AcceptOrdersDefaultValue)));

        print_using_fire_automatically_option.setChecked(preferences.getBoolean(OrderAlertService.PREFERENCE_KEY_FIRE_AUTO_PRINT,
                getResources().getBoolean(R.bool.PrintUsingFireAutomaticallyDefaultValue)));

        print_order_receipt_automatically_option.setChecked(preferences.getBoolean(OrderAlertService.PREFERENCE_KEY_ORDER_RECEIPT_AUTO_PRINT,
                getResources().getBoolean(R.bool.PrintOrderReceiptAutomaticallyDefaultValue)));
        selected_order_receipt_printer_view.setText(preferences.getString(OrderAlertService.PREFERENCE_KEY_ORDER_RECEIPT_PRINTER_NAME, "Select"));

        print_payment_receipt_automatically_option.setChecked(preferences.getBoolean(OrderAlertService.PREFERENCE_KEY_PAYMENT_RECEIPT_AUTO_PRINT,
                getResources().getBoolean(R.bool.PrintPaymentReceiptAutomaticallyDefaultValue)));
        selected_payment_receipt_printer_view.setText(preferences.getString(OrderAlertService.PREFERENCE_KEY_PAYMENT_RECEIPT_PRINTER_NAME, "Select"));

        order_alert_service_intent = new Intent(this, OrderAlertService.class);

        initMerchant();

        // set listeners
        order_alert_active_option.setOnCheckedChangeListener(this);
        full_screen_notification_option.setOnCheckedChangeListener(this);
        accept_order_option.setOnCheckedChangeListener(this);
        print_using_fire_automatically_option.setOnCheckedChangeListener(this);

        print_order_receipt_automatically_option.setOnCheckedChangeListener(this);
        selected_order_receipt_printer_view.setOnClickListener(this);

        print_payment_receipt_automatically_option.setOnCheckedChangeListener(this);
        selected_payment_receipt_printer_view.setOnClickListener(this);

        // show application version name and code
        TextView version_info = (TextView) findViewById(R.id.VersionInfoView);
        version_info.setText("Version: " + BuildConfig.VERSION_NAME + " build " + BuildConfig.VERSION_CODE);
    }

    private void initMerchant() {
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                cloverMerchant = new CloverMerchant(getApplicationContext());
                cloverMerchant.getAcceptOrder(new RequestCallback() {
                    @Override
                    public void onSuccessInt(int result) {
                        accept_order_option.setChecked(result == 1);
                    }

                    @Override
                    public void onSuccessString(String result) {
                    }

                    @Override
                    public void onError(VolleyError error) {
                    }
                });
                return null;
            }

        };
        task.execute();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {

        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);

        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }

        return false;
    }

    @Override
    protected void onResume() {

        super.onResume();

        // check if pending intent for token update already exists, only if not create a repeating alarm
        // (this should happen only if the alarm manager was not setup after device boot completed)

        TokenUpdateAlarmSetupReceiver.setupTokenUpdateAlarm(this);
    }

    private Boolean isNetworkAvailable() {
        ConnectivityManager cm =
                (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {

        if (compoundButton == order_alert_active_option) {

            if (checked) {
                startService(order_alert_service_intent);
            } else {
                stopService(order_alert_service_intent);
            }

            updateSetting(OrderAlertService.PREFERENCE_KEY_ENABLE_ORDER_ALERTS, checked);
        } else if (compoundButton == full_screen_notification_option) {
            updateSetting(OrderAlertService.PREFERENCE_KEY_FULL_SCREEN_NOTIFICATION, checked);
        }
        else if (compoundButton == accept_order_option) {

            cloverMerchant.setAcceptOrder(checked ? 1 : 0, new RequestCallback() {

                @Override
                public void onSuccessInt(int result) {
                    accept_order_option.setChecked(result == 1);
                }

                @Override
                public void onSuccessString(String result) {
                }

                @Override
                public void onError(VolleyError error) {
                    Toast.makeText(getApplicationContext(), "Cannot change accept order setting. Please try again.", Toast.LENGTH_SHORT).show();
                }
            });
        } else if (compoundButton == print_using_fire_automatically_option) {
            updateSetting(OrderAlertService.PREFERENCE_KEY_FIRE_AUTO_PRINT, checked);
        } else if (compoundButton == print_order_receipt_automatically_option) {
            updateSetting(OrderAlertService.PREFERENCE_KEY_ORDER_RECEIPT_AUTO_PRINT, checked);
        } else if (compoundButton == print_payment_receipt_automatically_option) {
            updateSetting(OrderAlertService.PREFERENCE_KEY_PAYMENT_RECEIPT_AUTO_PRINT, checked);
        }
    }

    @Override
    public void onClick(View v) {

        Intent selecte_printer_intent = new Intent(this,PrintersListActivity.class);

        switch (v.getId()) {
            case R.id.SelectedOrderPrinterView:
                this.startActivityForResult(selecte_printer_intent, REQUEST_ORDER_RECEIPT_PRINTER);
                break;
            case R.id.SelectedPaymentPrinterView:
                this.startActivityForResult(selecte_printer_intent, REQUEST_PAYMENT_RECEIPT_PRINTER);
                break;
        }
    }

    private void updateSetting(String setting, boolean value) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(setting, value);
        editor.apply();
    }

    public void openDashboard(View view) {
        cloverMerchant.getDashboardUrl(new RequestCallback() {
            @Override
            public void onSuccessInt(int result) {
            }

            @Override
            public void onSuccessString(String result) {
                Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);

                String url = result;
                intent.putExtra("url", url);
                startActivity(intent);
            }

            @Override
            public void onError(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Cannot open dashboard. Please try again", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK)
        {
            Printer printer = (Printer) data.getExtras().get(PrintersListActivity.EXTRA_PRINTER);

            if(printer != null) {

                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                SharedPreferences.Editor editor = preferences.edit();
                switch (requestCode) {
                    case REQUEST_ORDER_RECEIPT_PRINTER:
                        editor.putString(OrderAlertService.PREFERENCE_KEY_ORDER_RECEIPT_PRINTER_UUID, printer.uuid);
                        editor.putString(OrderAlertService.PREFERENCE_KEY_ORDER_RECEIPT_PRINTER_NAME, printer.name);
                        break;
                    case REQUEST_PAYMENT_RECEIPT_PRINTER:
                        editor.putString(OrderAlertService.PREFERENCE_KEY_PAYMENT_RECEIPT_PRINTER_UUID, printer.uuid);
                        editor.putString(OrderAlertService.PREFERENCE_KEY_PAYMENT_RECEIPT_PRINTER_NAME, printer.name);
                        break;
                }
                editor.apply();

                switch (requestCode) {
                    case REQUEST_ORDER_RECEIPT_PRINTER:
                        selected_order_receipt_printer_view.setText(printer.name);
                        break;
                    case REQUEST_PAYMENT_RECEIPT_PRINTER:
                        selected_payment_receipt_printer_view.setText(printer.name);
                        break;
                }

                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "------>Printer Category" + printer.getCategory());
                }
            }
        }
    }
}
