package com.fourleaflabs.orderalert.broadcastreceivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by georgsisow on 23.11.15.
 */
public class TokenUpdateAlarmSetupReceiver extends BroadcastReceiver {

    private static final String TAG = "TokenUpdate";

    @Override
    public void onReceive(Context context, Intent intent) {
        setupTokenUpdateAlarm(context);
    }

    public static void setupTokenUpdateAlarm(Context context) {

        Intent upload_token_intent = new Intent("com.fourleaflabs.orderalert.core.UPDATE_TOKEN");

        PendingIntent upload_token_broadcast = PendingIntent.getBroadcast(context, 0, upload_token_intent, PendingIntent.FLAG_NO_CREATE);

        // setup a new alarm only if the pending intent is not existing (an alarm with it was not created yet)
        if (upload_token_broadcast == null) {

            Log.i(TAG, "Periodic token update established.");

            upload_token_broadcast = PendingIntent.getBroadcast(context, 0, upload_token_intent, PendingIntent.FLAG_UPDATE_CURRENT);

            AlarmManager alarm_manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

            // start alarm manager to run every 4 hours
            alarm_manager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME, 14400000, 14400000, upload_token_broadcast);
        }
    }
}
