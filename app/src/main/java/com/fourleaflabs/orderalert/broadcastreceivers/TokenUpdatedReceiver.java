package com.fourleaflabs.orderalert.broadcastreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.fourleaflabs.orderalert.tasks.UploadCurrentTokenTask;

/**
 * Created by georgsisow on 22.11.15.
 */
public class TokenUpdatedReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        new UploadCurrentTokenTask(context).execute();
    }
}
