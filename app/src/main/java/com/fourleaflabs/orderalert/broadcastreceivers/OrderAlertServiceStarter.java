package com.fourleaflabs.orderalert.broadcastreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.fourleaflabs.orderalert.OrderAlertService;
import com.fourleaflabs.orderalert.R;

/**
 * Created by georgsisow on 24.11.15.
 */
public class OrderAlertServiceStarter extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        if (preferences.getBoolean(com.fourleaflabs.orderalert.OrderAlertService.PREFERENCE_KEY_ENABLE_ORDER_ALERTS,
                                                context.getResources().getBoolean(R.bool.OrderAlertsSettingDefaultValue))) {
            Intent order_alert_service_intent = new Intent(context, OrderAlertService.class);
            context.startService(order_alert_service_intent);
        }
    }

}
