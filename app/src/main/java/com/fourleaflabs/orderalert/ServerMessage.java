package com.fourleaflabs.orderalert;

import android.text.TextUtils;

/**
 * Message class that published from server through PubNub
 * No parent class or interface as we only have print message as this moment
 * Message format: EVENT_TYPE|TITLE|CREATED_TIME|TOTAL|CURRENCY|ORDER_ID
 * simple stupid is best
 */

public class ServerMessage {

    private String[] messageList;

    public ServerMessage(String message) {
        this.messageList = this.parseMessage(message);

        if (this.messageList.length != 6) {
            this.messageList = new String[]{"", "", "", "", "", ""};
            System.out.println("Invalid message format from server: " + message);
        }
    }

    public String[] parseMessage(String message) {
        String[] messages = message.split("\\|");
        return messages;
    }

    public String getAsString() {
        return TextUtils.join("|", this.messageList);
    }

    public String getType() {
        return this.messageList[0].replace("\"", "");
    }

    public String getOrderTitle() {
        return this.messageList[1];
    }

    public String getCreatedTime() {
        return this.messageList[2];
    }

    public String getOrderTotal() {
        return this.messageList[3];
    }

    public String getCurrency() {
        return this.messageList[4];
    }

    public String getOrderId() {
        return this.messageList[5].replace("\"", "");
    }



}
