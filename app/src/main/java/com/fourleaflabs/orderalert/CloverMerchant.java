package com.fourleaflabs.orderalert;

import android.accounts.Account;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.clover.sdk.util.CloverAccount;
import com.clover.sdk.util.CloverAuth;
import com.fourleaflabs.orderalert.interfaces.RequestCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CloverMerchant {

    private Context context;
    private String merchant_api_url;
    protected String token;

    public CloverMerchant(Context context) {
        this.context = context;

        try {
            this.setProps();
        } catch (OperationCanceledException e) {
            e.printStackTrace();
        } catch (AuthenticatorException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setProps() throws OperationCanceledException, AuthenticatorException, IOException {
        Account account = CloverAccount.getAccount(this.context);
        CloverAuth.AuthResult auth_result = CloverAuth.authenticate(this.context, account);
        String merchantId = auth_result.merchantId;
        this.merchant_api_url = BuildConfig.MerchantUrl + "/" + merchantId;
        this.token = auth_result.authToken;
    }

    public void getAcceptOrder(final RequestCallback callback) {

        RequestQueue requestQueue = Volley.newRequestQueue(this.context);
        final String token = this.token;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, this.merchant_api_url + "/setting",
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            callback.onSuccessInt(response.getInt("acceptOrder"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-token", token);

                return params;
            }
        };
        requestQueue.add(request);
    }

    public void setAcceptOrder(int acceptOrder, final RequestCallback callback) {
        final RequestQueue requestQueue = Volley.newRequestQueue(this.context);
        final String token = this.token;
        final Context thisContext = this.context;

        JSONObject payload = new JSONObject();
        try {
            payload.put("acceptOrder", acceptOrder);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, this.merchant_api_url + "/setting", payload,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            int acceptOrder = response.getInt("acceptOrder");
                            callback.onSuccessInt(acceptOrder);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        callback.onError(error);
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-token", token);

                return params;
            }

        };
        requestQueue.add(request);
    }

    public void getDashboardUrl(final RequestCallback callback) {

        RequestQueue requestQueue = Volley.newRequestQueue(this.context);
        final String token = this.token;
        final String merchant_api_url = this.merchant_api_url;
        final Context thisContext = this.context;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, this.merchant_api_url + "/dashboard",
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String token = response.getString("url");
                            String url = merchant_api_url + "/dashboard/open?token=" + token;
                            System.out.println(url);
                            callback.onSuccessString(url);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        callback.onError(error);
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-token", token);

                return params;
            }
        };
        requestQueue.add(request);
    }


}
