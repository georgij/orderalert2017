package com.fourleaflabs.orderalert.addapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import com.clover.sdk.v1.printer.Printer;
import com.fourleaflabs.orderalert.R;
import com.fourleaflabs.orderalert.interfaces.CallBackActivity;

import java.util.List;

/**
 * Created by android6 on 23/12/14.
 */
 public class PrinterAdapter extends BaseAdapter {
    CallBackActivity Refresh;
    List<Printer> Result;
    Activity activity;
    LayoutInflater inflater;
    public PrinterAdapter(Activity activity, List<Printer> Result, CallBackActivity Refresh)
    {
        this.Result=Result;
        this.Refresh=Refresh;
        this.activity=activity;
        inflater=  activity.getLayoutInflater();
    }
    @Override
    public int getCount() {
        return Result.size();

    }

    @Override
    public Printer getItem(int position) {
        if(this.Result.size()>position)
        return this.Result.get(position);
        else
            return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null)
        {
            try {
                convertView = inflater.inflate(R.layout.orderalert_item_printer, null);
            }
            catch (OutOfMemoryError e1) {
                e1.printStackTrace();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            holder = new ViewHolder();
            holder.name = (TextView)convertView.findViewById(R.id.OrderAlert_item_printerName);
            holder.radioButton=(RadioButton)convertView.findViewById(R.id.OrderAlert_item_printerRd);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }
        if(Result!=null)
        {
            Printer printer=Result.get(position);
            holder.name.setText(printer.getName());
        }

        holder.name.setOnClickListener(new TittleCLiked(position,holder.radioButton));
        return convertView;
    }
    class ViewHolder{
        TextView name;
        RadioButton radioButton;

    }
    class TittleCLiked implements View.OnClickListener
    {
        int position;
        RadioButton rd;
        public TittleCLiked(int position,RadioButton Rd)
        {
            this.position=position;
            this.rd=Rd;
        }
        @Override
        public void onClick(View v) {
            rd.setChecked(true);
           Refresh.CallBackActivity(position);
        }
    }
}
